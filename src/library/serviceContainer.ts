import SwapiRepository from '@/library/api/swapiRepository';
import ApiConfig from '@/config/apiConfig';

interface IServiceContainer {
  swapiRepository: SwapiRepository
}

const serviceContainer: IServiceContainer = {
  swapiRepository: new SwapiRepository(ApiConfig.apiUrl),
};

export default serviceContainer;

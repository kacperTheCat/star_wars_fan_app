import SwapiRepositoryInterface, { Film } from '@/library/api/swapiRepositoryInterface';

export default class SwapiRepository implements SwapiRepositoryInterface {
  constructor(public baseUrl: string) {
  }

  public filmListStorageKey = 'filmList'

  public async getMovieList() {
    const response = await fetch(`${this.baseUrl}films/`)
      .then(res => res.json())
      .then(films => films);
    return response.result;
  }

  public addFilmsToLocalStorage(films: Film[]) {
    localStorage.setItem(this.filmListStorageKey, JSON.stringify(films));
  }

  public getFilmsFromLocalStorage() {
    return JSON.parse(localStorage[this.filmListStorageKey]);
  }
}

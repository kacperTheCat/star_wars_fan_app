/* eslint-disable camelcase */
export class Film {
  constructor(
    public description: string,
    public properties: FilmProperties,
    public uid: string,
  ) {}
}

export interface FilmProperties {
  created: string
  edited: string
  producer: string
  title: string
  episode_id: number
  director: string
  opening_crawl: string
  url: string
}

export default interface SwapiRepositoryInterface {
  getMovieList(): Promise<Film[]>,
  addFilmsToLocalStorage(Films: Film[]): void,
  getFilmsFromLocalStorage(): Film[]
};

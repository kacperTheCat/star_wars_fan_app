import { Film } from '@/library/api/swapiRepositoryInterface';

class FilmsStateModel {
  constructor(
    public films: Film[],
    public loading: boolean,
    public error: boolean,
    public filteredListOfFilms: Film[],
  ) { }
}

export default FilmsStateModel;

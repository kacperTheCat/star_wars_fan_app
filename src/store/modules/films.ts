import { Film } from '@/library/api/swapiRepositoryInterface';
import serviceContainer from '@/library/serviceContainer';
import FilmsStateModel from '@/structures/films';

const moduleName = 'films';

const GETTERS = {
  GET_MOVIE_NAMES: 'getMovieNames',
};

const ACTIONS = {
  GET_FILMS_ACTION: 'getFilmsAction',
  GET_MOVIE_BY_TITLE_ACTION: 'getMovieByTitleAction',
  SORT_FILMS_BY_PROPERTY_ACTION: 'sortFilmsByPropertyAction',
};

const MUTATIONS = {
  SET_FILMS: 'setFilms',
  SET_LOADING: 'setLoading',
  SET_ERROR: 'setError',
  SET_SELECTED_FILM: 'setSelectedFilm',
  SORT_FILMS_BY_PROPERTY: 'sortFilmsByProperty',
};

type FilmSortingProperties = 'title' | 'episode_id'

const state: FilmsStateModel = {
  films: [], // base data
  loading: true,
  error: false,
  filteredListOfFilms: [], // data for any operations
};

const getters = {
  [GETTERS.GET_MOVIE_NAMES]:
    (state: FilmsStateModel) => state.films.map(film => film.properties.title),
};

const actions = {
  async [ACTIONS.GET_FILMS_ACTION]({ commit }: any) {
    commit(MUTATIONS.SET_LOADING, true);
    try {
      if (localStorage[serviceContainer.swapiRepository.filmListStorageKey]) {
        const films = serviceContainer.swapiRepository.getFilmsFromLocalStorage();
        commit(MUTATIONS.SET_FILMS, films);
      } else {
        const films = await serviceContainer.swapiRepository.getMovieList();
        serviceContainer.swapiRepository.addFilmsToLocalStorage(films);
        commit(MUTATIONS.SET_FILMS, films);
      }
    } catch (error) {
      commit(MUTATIONS.SET_ERROR, error); // let's assume, error is a boolean 💩💩💩
    } finally {
      commit(MUTATIONS.SET_LOADING, false);
    }
  },
  [ACTIONS.GET_MOVIE_BY_TITLE_ACTION]({ commit }: any, filmTitle: string) {
    commit(MUTATIONS.SET_SELECTED_FILM, filmTitle);
  },
  [ACTIONS.SORT_FILMS_BY_PROPERTY_ACTION]({ commit }: any, filmTitle: string) {
    commit(MUTATIONS.SORT_FILMS_BY_PROPERTY, filmTitle);
  },
};

const mutations = {
  [MUTATIONS.SET_FILMS](state: FilmsStateModel, films: Film[]) {
    state.films = films;
    state.filteredListOfFilms = films;
  },
  [MUTATIONS.SET_LOADING](state: FilmsStateModel, isLoading: boolean) {
    state.loading = isLoading;
  },
  [MUTATIONS.SET_ERROR](state: FilmsStateModel, error: boolean) {
    state.error = error;
  },
  [MUTATIONS.SET_SELECTED_FILM](state: FilmsStateModel, filmTitle: string) {
    const selectedFilm = state.films.find(film => film.properties.title === filmTitle);
    state.filteredListOfFilms = selectedFilm ? [selectedFilm] : state.films;
  },
  [MUTATIONS.SORT_FILMS_BY_PROPERTY](state: FilmsStateModel, property: FilmSortingProperties) {
    state.filteredListOfFilms = state.films
      .sort((a: Film, b:Film) => (a.properties[property] < b.properties[property] ? -1 : 1));
  },
};

export const Films = {
  NAME: moduleName,
  ACTIONS,
  GETTERS,
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
};

import Vuex from 'vuex';
import films from '@/store/modules/films';
import Vue from 'vue';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    films,
  },
});

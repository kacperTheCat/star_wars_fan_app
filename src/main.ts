import Vue from 'vue';
import 'vuetify/dist/vuetify.min.css';
import Vuetify from 'vuetify';
import App from './App.vue';
import router from './router';
import './registerServiceWorker';
import store from '@/store/store';

Vue.config.productionTip = false;


Vue.use(Vuetify);
new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
